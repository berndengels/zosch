<?php
/**
 * EventForm.php
 *
 * @author    Bernd Engels
 * @created   30.01.19 22:55
 * @copyright Webwerk Berlin GmbH
 */

namespace App\Forms;

//use App\Models\Role;

use App\Models\MusicStyle;
use Kris\LaravelFormBuilder\Form;
use Kris\LaravelFormBuilder\Field;

class UserForm extends MainForm
{
    protected $formOptions = [
        'method' => 'POST',
        'url' => '/admin/users/store/',
    ];

    public function buildForm()
    {
		$model	= $this->getModel() ?: null;
		$id     = $model ? $this->getModel()->id : null;

        $this
            ->add('id', Field::HIDDEN)
            ->add('enabled', Field::CHECKBOX);

		if( auth()->user() && auth()->user()->is_super_admin ) {
			$this->add('is_super_admin', Field::CHECKBOX);
		}

		$this->add('username', Field::TEXT, [
                'rules' => 'required|min:3|max:50'
            ])
			->add('email', Field::EMAIL, [
                'rules' => 'required|min:5|max:50'
            ])
            ->add('password', Field::PASSWORD, [
//                'rules' => 'nullable|string|min:5'
            ])
			->add('musicStyles', Field::ENTITY, [
				'class' => MusicStyle::class,
				'label' => 'Musik Stile für Bandanfragen',
				'empty_value'  => 'Bitte wählen ...',
				'selected' => null,
				'multiple' => true,
				'help_block' => [
					'text' => 'Bandanfragen werden an Eure Musik-Stile weitergeleitet',
					'tag' => 'p',
					'attr' => ['class' => 'help-block']
				],
				'query_builder' => function (MusicStyle $item) {
					return $item->orderBy('name')->get();
				}
			])
        ;
		$this->addSubmits();

        if( $id > 0 ) {
            $this->formOptions['url'] .= $id;
        }

    }
}