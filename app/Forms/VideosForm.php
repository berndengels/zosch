<?php
/**
 * ImageForm.php
 *
 * @author    Bernd Engels
 * @created   25.02.19 14:47
 * @copyright Webwerk Berlin GmbH
 */
namespace App\Forms;

use App\Forms\MediaForm;

class VideosForm extends MediaForm
{
    public function buildForm()
    {
        parent::buildForm();
    }
}