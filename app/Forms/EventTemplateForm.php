<?php
/**
 * EventForm.php
 *
 * @author    Bernd Engels
 * @created   30.01.19 22:55
 * @copyright Webwerk Berlin GmbH
 */
namespace App\Forms;

use App\Models\Image;
use App\Forms\ImagesForm;
use Carbon\Carbon;
use Kris\LaravelFormBuilder\Form;
use Kris\LaravelFormBuilder\Field;
use App\Forms\Fields\DatePickerType;

class EventTemplateForm extends MainForm
{
    protected $formOptions = [
        'method' => 'POST',
        'id'    => 'frmDropzone',
        'class' => 'dropzone',
        'url' => '/admin/eventsTemplate/store/',
    ];

    public function buildForm()
    {
        $model      = $this->getModel() ?: null;
        $id         = $model ? $model->id : null;
        $categoryId = ($model && $model->category) ? $model->category->id : null;
        $themeId    = ($model && $model->theme) ? $model->theme->id : null;
        $this
            ->add('id', Field::HIDDEN, [
				'attr' => [
					'id' => 'id',
				]
			])
            ->add('category', Field::ENTITY, [
                'class' => 'App\Models\Category',
                'selected'  => $categoryId,
                'empty_value'  => $id ? null : 'Bitte wählen ...',
            ])
            ->add('theme', Field::ENTITY, [
                'class' => 'App\Models\Theme',
                'selected'  => $themeId,
                'empty_value'  => $id ? null : 'Bitte wählen ...',
            ])
            ->add('title', Field::TEXT, [
                'rules' => 'required|min:5'
            ])
            ->add('subtitle', Field::TEXT, [
                'rules' => 'max:255'
            ])
            ->add('description', Field::TEXTAREA, [
                'attr'  => ['id' => 'tinymce'],
            ])
            ->add('links', Field::TEXTAREA, [
                'value' => function ($val = null) {
                    return is_array($val) ? implode("\n",$val) : '';
                }
            ]);

        if( $model && $model->images && $model->images->count() > 0 ) {
            $this->add('images', 'collection', [
                'prototype'     => true,
                'type'          => 'form',
                'label_show'    => false,
                'wrapper'       => [
					'id'	=> 'images',
                    'class' => 'form-group event-images collapseImages multi-collapse show',
                ],
                'options'   => [
                    'class' => ImagesForm::class,
                    'label' => false,
                ]
            ]);
        }
		$this->addSubmits();

		$id = $this->getField('id')->getValue();
        if( $id > 0 ) {
            $this->formOptions['url'] .= $id;
        }
    }
}