<?php
/**
 * PeriodicDate.php
 *
 * @author    Bernd Engels
 * @created   30.03.19 15:20
 * @copyright Webwerk Berlin GmbH
 */

namespace App\Libs;

use Carbon\Carbon;
use App\Helper\MyDate;
use Carbon\CarbonPeriod;
use Carbon\CarbonInterval;
use App\Models\PeriodicEvent;

class EventDateTime extends Carbon
{
	protected $tz = 'Europe/Berlin';
    protected $today;
	protected $todayWeekday;
    protected $endDate;
    protected $endDateIntern;
    protected $monthLimit = 2;
    protected $monthLimitIntern = 6;
    protected $weekDaysByName = [];
    protected $validInMonthKeys = ['first','second','third','last'];
    protected $validWeekKeys = [1,2,3,4];

    public function __construct($time = null, $tz = null)
    {
        $this->today    		= MyDate::getUntilValidDate();
		$this->todayWeekday    	= $this->today->format('l');
        $this->endDate  		= $this->today->copy()->addMonth($this->monthLimit);
        $this->endDateIntern    = $this->today->copy()->addMonth($this->monthLimitIntern);
        $this->weekDaysByName   = array_flip(parent::$days);
    }

    public function getPeriodicEventDates($positionSearchKey, $weekday, $formated = true, $isPublic = false)
    {
        $weekday = ucfirst($weekday);
        $dates = null;
		if( in_array((int) $positionSearchKey, $this->validWeekKeys) ) {
			$dates = $this->getPeriodicWeekDates($weekday, $positionSearchKey, $formated, $isPublic);
		} elseif(in_array($positionSearchKey, $this->validInMonthKeys)) {
			$dates = $this->getPeriodicInMonth($weekday,$positionSearchKey, $formated, $isPublic);
		}

        return $dates;
    }

    public function getPeriodicWeekDates($weekday, $weekInterval = 1, $formated = true, $isPublic = false)
    {
		if($weekday === $this->todayWeekday) {
			$start  = $this->today->copy();
		} else {
			$start  = $this->today->copy()->next($this->weekDaysByName[ucfirst($weekday)]);
		}
        $arr    = $formated ? [$start->format('Y-m-d')] : [$start];
        /**
         * @var $date Carbon
         */
        do {
            $date = $start->addWeeks($weekInterval);
            $arr[] = $formated ? $date->format('Y-m-d') : $date;
        }
        while ( $date->lt($isPublic ? $this->endDate : $this->endDateIntern) );
        return $arr;
    }

    public function getPeriodicInMonth($weekday, $position, $formated = true, $isPublic = false) {
        $wd     = $this->weekDaysByName[ucfirst($weekday)];
		$start  = $this->getPositionedDate($this->today->copy(), $wd, $position);

        $validStart = $start->gte($this->today->copy());
        $arr    = $validStart ? [$formated ? $start->format('Y-m-d') : $start] : [];
        /**
         * @var $date Carbon
         */
        do {
            $date = $start->addMonth(1);
            $date = $this->getPositionedDate($date, $wd, $position);
            $arr[] = $formated ? $date->format('Y-m-d') : $date;
        }
        while ( $date->lt($isPublic ? $this->endDate : $this->endDateIntern) );
        return $arr;
    }

    public function getPositionedDate($date, $wd, $position){
        switch($position) {
            case 'first':
                $date = $date->firstOfMonth($wd);
                break;
            case 'second':
                $date = $date->nthOfMonth(2, $wd);
                break;
            case 'third':
                $date = $date->nthOfMonth(3, $wd);
                break;
            case 'last':
                $date = $date->lastOfMonth($wd);
                break;
        }
        return $date;
    }
}
