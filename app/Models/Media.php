<?php

namespace App\Models;

use App\Models\Ext\HasUser;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Media
 */
class Media extends Model
{
	use HasUser;

    protected $fillable = ['title'];

	public function page()
	{
		return $this->belongsTo('App\Models\Page');
	}

    public function event()
    {
        return $this->belongsTo('App\Models\Event');
    }

    public function eventPeriodic()
    {
        return $this->belongsTo('App\Models\EventPeriodic');
    }
}
