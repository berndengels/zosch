<?php

namespace App\Models;

use App\Models\Image;
use App\Models\Ext\HasUser;
use Illuminate\Database\Eloquent\Model;
use App\Repositories\EventPeriodicRepository;
use Kyslik\ColumnSortable\Sortable;

/**
 * Class Event
 */
class EventPeriodic extends Model
{
	use Sortable, HasUser;

	public $sortable = [
		'category',
		'title',
		'created_at',
	];

	/**
     * @var \Illuminate\Support\Collection;
     */
    private $_eventDates = null;
	public $descriptionSanitized;

    /**
     * @var string
     */
    protected $table = 'event_periodic';
    /**
     * @var array
     */
    protected $fillable = ['title', 'subtitle'];

    protected $dates = ['start_date','created_at','updated_at'];

//    protected $appends = array('eventDates');

    public static function boot() {
        parent::boot();
        $repo   = new EventPeriodicRepository();

        EventPeriodic::retrieved(function($entity) use ($repo) {
            $dates  = $repo->getPeriodicDates($entity, true, true);
            $entity->_eventDates = $dates;
			$wrapper = '<div class="row embed-responsive-wrapper text-center"><div class="embed-responsive embed-responsive-16by9 m-0 p-0">%%</div></div>';
			$entity->descriptionSanitized = preg_replace("/(<iframe[^>]+><\/iframe>)/i", str_replace('%%','$1', $wrapper), $entity->description);
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function periodicPosition()
    {
        return $this->belongsTo('App\Models\PeriodicPosition');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function periodicWeekday()
    {
        return $this->belongsTo('App\Models\PeriodicWeekday');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function theme()
    {
        return $this->belongsTo('App\Models\Theme');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        return $this->hasMany('App\Models\Images');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function audios()
    {
        return $this->hasMany('App\Models\Audios');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function videos()
    {
        return $this->hasMany('App\Models\Videos');
    }

    /**
     * @param string $value
     * @return array
     */
    public function getLinksAttribute($value = '')
    {
        if('' !== $value) {
			return collect(preg_split("/[\n\r]+/", $value));
        }
    }

	/**
	 * @param string $value
	 * @return array
	 */
	public function getDescriptionAttribute($value = '')
	{
		if('' !== $value) {
			return preg_replace(['/^<p>(<br[ ]?[\/]?>){1,}/i','/(<br[ ]?[\/]?>){1,}<\/p>$/i'],['<p>','</p>'], trim($value));
		}
	}

	public function getEventDates()
    {
        return $this->_eventDates;
    }
}
