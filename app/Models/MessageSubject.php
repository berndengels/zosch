<?php

namespace App\Models;

use App\Models\MusicStyle;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\MessageSubject
 */
class MessageSubject extends Model
{
    protected $table = 'message_subject';
    protected $fillable = ['name'];
    public $timestamps = false;

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function messages()
	{
		return $this->hasMany(Message::class);
	}

}
