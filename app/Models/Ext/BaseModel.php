<?php
/**
 * BaseModel.php
 *
 * @author    Bernd Engels
 * @created   13.03.19 14:54
 * @copyright Webwerk Berlin GmbH
 */
namespace App\Models\Ext;

use Illuminate\Database\Eloquent\Model;
use App\Libs\Venturecraft\Revisionable\RevisionableTrait;
use Fico7489\Laravel\RevisionableUpgrade\Traits\RevisionableUpgradeTrait;

/**
 * App\Models\Ext\BaseModel
 */
class BaseModel extends Model
{
    use RevisionableTrait;
    use RevisionableUpgradeTrait;

    protected $revisionCreationsEnabled = true;
	public $paginationLimit = 10;

	public function __construct(array $attributes = []) {
		parent::__construct($attributes);
	}

}