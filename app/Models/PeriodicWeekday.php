<?php
/**
 * PeriodicPosition.php
 *
 * @author    Bernd Engels
 * @created   13.03.19 16:25
 * @copyright Webwerk Berlin GmbH
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PeriodicWeekday
 */
class PeriodicWeekday extends Model
{
    /**
     * @var string
     */
    protected $table = 'periodic_weekday';
    /**
     * @var array
     */
    protected $fillable = ['name_de', 'name_en', 'short'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function eventPeriodic()
    {
        return $this->hasMany('App\Models\EventPeriodic');
    }
}