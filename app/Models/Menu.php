<?php

namespace App\Models;

use Kalnoy\Nestedset\NodeTrait;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
	use NodeTrait;

    protected $table = 'menu';
    public $timestamps = false;
    protected $fillable = ['parent_id','name','lvl','lft','rgt','url','menuItemType','is_published'];

	public function menuItemType()
	{
		return $this->belongsTo(MenuItemType::class);
	}

	public function getLftName()
    {
        return 'lft';
    }

    public function getRgtName()
    {
        return 'rgt';
    }

    public function getDepthName()
    {
        return 'lvl';
    }

    public function getParentIdName()
    {
        return 'parent_id';
    }

// Specify parent id attribute mutator
    public function setParentAttribute($value)
    {
        $this->setParentIdAttribute($value);
    }
}
