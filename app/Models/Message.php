<?php

namespace App\Models;

use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Address
 */
class Message extends Model
{
	use Sortable;

    protected $table = 'message';
    protected $fillable = ['music_style_id','email','name','message'];
    public $timestamps = true;

	const UPDATED_AT = null;

	public $sortable = [
		'musicStyle',
		'name',
		'email',
		'created_at',
	];

	public function messageSubject()
	{
		return $this->belongsTo(MessageSubject::class);
	}

	public function musicStyle()
	{
		return $this->belongsTo(MusicStyle::class);
	}
}
