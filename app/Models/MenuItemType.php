<?php
/**
 * MenuItemType.php
 *
 * @author    Bernd Engels
 * @created   05.04.19 16:21
 * @copyright Webwerk Berlin GmbH
 */
namespace App\Models;

use App\Models\Menu;
use Illuminate\Database\Eloquent\Model;

class MenuItemType extends Model {

	protected $table = 'menu_item_type';
	public $timestamps = false;

	protected $fillable = ['type','name'];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function menus()
	{
		return $this->hasMany(Menu::class);
	}
}