<?php

namespace App\Models;

use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\AddressCategory
 */
class AddressCategory extends Model
{
	use Sortable;

	protected $table = 'address_category';
    protected $fillable = ['name'];
    public $timestamps = false;

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function addresses()
	{
		return $this->hasMany(AddressCategory::class, 'address_category_id', 'id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function newsletters()
	{
		return $this->hasMany(Newsletter::class, 'tag_id', 'tag_id');
	}
}
