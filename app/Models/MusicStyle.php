<?php
namespace App\Models;

use App\Models\Message;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Kyslik\ColumnSortable\Sortable;

/**
 * App\Models\MusicStyle
 */
class MusicStyle extends Model
{
	use Sortable;

	protected $table = 'music_style';
    protected $fillable = ['name','slug'];
    public $timestamps = false;
	public $sortable = [
		'name',
	];

	public static function boot() {
		parent::boot();
		self::creating(function($model) {
			$model->slug = Str::slug(str_replace('.','_',$model->name), '-', 'de');
		});
		self::saving(function($model) {
			$model->slug = Str::slug(str_replace('.','_',$model->name), '-', 'de');
		});
		self::updating(function($model) {
			$model->slug = Str::slug(str_replace('.','_',$model->name), '-', 'de');
		});
	}

	public function users()
	{
		return $this->belongsToMany(User::class);
	}

	/**
     * @return HasMany
     */
    public function messages()
    {
        return $this->hasMany(Message::class);
    }
}
