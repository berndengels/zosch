<?php

namespace App\Models;

//use App\Models\Role;
//use Spatie\Permission\Traits\HasRoles;
use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Kyslik\ColumnSortable\Sortable;

/**
 * App\Models\User
 */
class User extends Authenticatable implements JWTSubject
{
//    use Notifiable, HasRoles;
    use Notifiable, Sortable;
	public $sortable = [
		'username',
		'email',
		'last_login',
		'enabled',
	];

    /**
     * @var string
     */
    protected $table = 'my_user';
	protected  $primaryKey = 'id';
	public $incrementing = true;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = ['last_login','created_at','updated_at'];

	public function musicStyles()
	{
		return $this->belongsToMany(MusicStyle::class, 'user_music_styles', 'user_id', 'music_style_id');
	}

	public function setLastLogin()
	{
		$now = Carbon::now('Europe/berlin');
		$this->last_login = $now->format('Y-m-d H:i:s');
		return $this;
	}

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function eventsCreated()
	{
		return $this->hasMany('App\Models\Event', 'created_by', 'id');
	}

	public function eventsUpdated()
	{
		return $this->hasMany('App\Models\Event', 'updated_by', 'id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function eventTemplatesCreated()
	{
		return $this->hasMany('App\Models\EventTemplate', 'created_by', 'id');
	}

	public function eventTemplatesUpdated()
	{
		return $this->hasMany('App\Models\EventTemplate', 'updated_by', 'id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function eventsPeriodicCreated()
	{
		return $this->hasMany('App\Models\EventPeriodic', 'created_by', 'id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function eventsPeriodicUpdated()
	{
		return $this->hasMany('App\Models\EventPeriodic', 'updated_by', 'id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function pagesCreated()
	{
		return $this->hasMany('App\Models\Pages', 'created_by', 'id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function pagesUpdated()
	{
		return $this->hasMany('App\Models\Pages', 'updated_by', 'id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function newsletterCreated()
	{
		return $this->hasMany('App\Models\Newsletter', 'created_by', 'id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function newsletterUpdated()
	{
		return $this->hasMany('App\Models\Newsletter', 'updated_by', 'id');
	}
}
