<?php
/**
 * PeriodicPosition.php
 *
 * @author    Bernd Engels
 * @created   13.03.19 16:25
 * @copyright Webwerk Berlin GmbH
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PeriodicPosition
 */
class PeriodicPosition extends Model
{
    /**
     * @var string
     */
    protected $table = 'periodic_position';
    /**
     * @var array
     */
    protected $fillable = ['name', 'search_key'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function eventPeriodic()
    {
        return $this->hasMany('App\Models\EventPeriodic');
    }


}