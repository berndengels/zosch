<?php
/**
 * NewsletterStatus.php
 *
 * @author    Bernd Engels
 * @created   15.06.19 13:01
 * @copyright Webwerk Berlin GmbH
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewsletterStatus extends Model
{
	/**
	 * @var string
	 */
	protected $table = 'newsletter_status';
	/**
	 * @var array
	 */
	protected $fillable = ['name'];

}