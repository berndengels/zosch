<?php
/**
 * Newsletter.php
 *
 * @author    Bernd Engels
 * @created   15.06.19 12:50
 * @copyright Webwerk Berlin GmbH
 */

namespace App\Models;

use App\Models\Ext\HasUser;
use Illuminate\Database\Eloquent\Model;

class Newsletter extends Model {
	use HasUser;

	/**
	 * @var string
	 */
	protected $table = 'newsletter';
	/**
	 * @var array
	 */
	protected $fillable = ['id'];
	protected $dates = ['created_at','updated_at'];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function tag()
	{
		return $this->belongsTo(AddressCategory::class, 'tag_id', 'tag_id');
	}
}
