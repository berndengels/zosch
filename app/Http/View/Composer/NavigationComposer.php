<?php
/**
 * NavigationComposer.php
 *
 * @author    Bernd Engels
 * @created   10.04.19 04:27
 * @copyright Webwerk Berlin GmbH
 */

namespace App\Http\View\Composer;

use App\Models\Menu;
use Illuminate\View\View;
use App\Models\MenuItemType;

class NavigationComposer {

	public function compose(View $view)
	{
		$topMenuType	= MenuItemType::where('type', 'topMenuRoot')->first();
		$bottomMenuType	= MenuItemType::where('type', 'bottomMenuRoot')->first();
		$topMenu 		= Menu::where('menu_item_type_id', $topMenuType->id)->first();
		$bottomMenu 	= Menu::where('menu_item_type_id', $bottomMenuType->id)->first();

		$view->with('topMenu', Menu::where('is_published',1)->defaultOrder()->descendantsOf($topMenu->id)->toTree());
		$view->with('bottomMenu', Menu::where('is_published',1)->defaultOrder()->descendantsOf($bottomMenu->id)->where('is_published',1)->toTree());
	}
}