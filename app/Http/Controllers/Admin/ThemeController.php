<?php
/**
 * UserController.php
 *
 * @author    Bernd Engels
 * @created   28.02.19 17:17
 * @copyright Webwerk Berlin GmbH
 */
namespace App\Http\Controllers\Admin;

use App\Models\Theme;
use App\Forms\ThemeForm;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class ThemeController extends MainController
{
    use FormBuilderTrait;

    static protected $model = Theme::class;
    static protected $form = ThemeForm::class;

    public function store( Request $request, $id = 0 )
    {
        $form   = $this->form(ThemeForm::class);
        $entity = ($id > 0) ? Theme::find($id) : new Theme();

        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $entity->name = $form->name->getRawValue();
        $entity->slug    = Str::slug($form->name->getRawValue());
        $entity->is_published  = $form->is_published->getRawValue();

        try {
            $entity->saveOrFail();
            $id = $entity->id;
        } catch (\Exception $e) {
            die($e->getMessage());
        }

        switch($request->submit) {
            case 'save':
                return redirect()->route('admin.themeEdit', ['id' => $id]);
            case 'saveAndBack':
            default:
                return redirect()->route('admin.themeList');
        }
    }
}
