<?php
/**
 * UserController.php
 *
 * @author    Bernd Engels
 * @created   28.02.19 17:17
 * @copyright Webwerk Berlin GmbH
 */
namespace App\Http\Controllers\Admin;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\MusicStyle;
use App\Forms\MusicStyleForm;
use Illuminate\Support\Facades\DB;
use Kris\LaravelFormBuilder\FormBuilder;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class MusicStyleController extends MainController
{
    use FormBuilderTrait;

    static protected $model = MusicStyle::class;
    static protected $form = MusicStyleForm::class;
	static protected $orderBy = 'name';

	public function store( Request $request, $id = 0 )
    {
        $form   = $this->form(self::$form);
        $entity = ($id > 0) ? MusicStyle::find($id) : new MusicStyle();

        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $entity->name = $form->name->getRawValue();
        $entity->slug = Str::slug($form->name->getRawValue());

        try {
            $entity->saveOrFail();
            $id = $entity->id;
        } catch (\Exception $e) {
            die($e->getMessage());
        }

        switch($request->submit) {
            case 'save':
                return redirect()->route('admin.musicStyleEdit', ['id' => $id]);
            case 'saveAndBack':
            default:
                return redirect()->route('admin.musicStyleList');
        }
    }
}
