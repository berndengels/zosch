<?php
/**
 * AddressCategory.php
 *
 * @author    Bernd Engels
 * @created   15.06.19 19:19
 * @copyright Webwerk Berlin GmbH
 */

namespace App\Http\Controllers\Admin;

use App\Entities\Newsletter\ContactEntity;
use Illuminate\Http\Request;
use App\Models\AddressCategory;
use App\Forms\AddressCategoryForm;
use App\Repositories\NewsletterRepository;
use Kris\LaravelFormBuilder\FormBuilder;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class AddressCategoryController extends MainController {

	use FormBuilderTrait;

	static protected $model	= AddressCategory::class;
	static protected $form	= AddressCategoryForm::class;

	public function store( Request $request, $id = 0 )
	{
		$form   = $this->form(AddressCategoryForm::class);
		$entity = ($id > 0) ? AddressCategory::find($id) : new AddressCategory();
		$repository = new NewsletterRepository();

		if (!$form->isValid()) {
			return redirect()->back()->withErrors($form->getErrors())->withInput();
		}

		$entity->name = $form->name->getRawValue();

		try {
			$response = null;
			if(!$entity->tag_id) {
				$listTag = $repository->getListTagByName($entity->name);
				if(!$listTag) {
					$response = $repository->createListTag($entity->name);
					$entity->tag_id = $response['id'];
				} else {
					$entity->tag_id = $listTag['id'];
				}
			}
			$entity->saveOrFail();
			$id = $entity->id;

		} catch (\Exception $e) {
			die($e->getMessage());
		}

		switch($request->submit) {
			case 'save':
				return redirect()->route('admin.addressCategoryEdit', ['id' => $id]);
			case 'saveAndBack':
			default:
				return redirect()->route('admin.addressCategoryList');
		}
	}


}