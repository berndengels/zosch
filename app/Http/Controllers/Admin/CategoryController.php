<?php
/**
 * UserController.php
 *
 * @author    Bernd Engels
 * @created   28.02.19 17:17
 * @copyright Webwerk Berlin GmbH
 */
namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Forms\CategoryForm;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class CategoryController extends MainController
{
    use FormBuilderTrait;

    static protected $model	= Category::class;
    static protected $form	= CategoryForm::class;

	public function store( Request $request, $id = 0 )
    {
        $form   = $this->form(CategoryForm::class);
        $entity = ($id > 0) ? Category::find($id) : new Category();

        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $entity->name = $form->name->getRawValue();
        $entity->slug = Str::slug($form->name->getRawValue());
		$entity->icon = $form->icon->getRawValue();
		$entity->default_time	= $form->default_time->getRawValue();
		$entity->default_price	= $form->default_price->getRawValue();
        $entity->is_published	= $form->is_published->getRawValue();

        try {
            $entity->saveOrFail();
            $id = $entity->id;
        } catch (\Exception $e) {
            die($e->getMessage());
        }

        switch($request->submit) {
            case 'save':
                return redirect()->route('admin.categoryEdit', ['id' => $id]);
            case 'saveAndBack':
            default:
                return redirect()->route('admin.categoryList');
        }
    }
}
