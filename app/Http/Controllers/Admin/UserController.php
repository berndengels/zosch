<?php
/**
 * UserController.php
 *
 * @author    Bernd Engels
 * @created   28.02.19 17:17
 * @copyright Webwerk Berlin GmbH
 */
namespace App\Http\Controllers\Admin;

use App\Models\MusicStyle;
use Hash;
use App\Forms\UserForm;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\User;
use Kris\LaravelFormBuilder\FormBuilder;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class UserController extends MainController
{
    use FormBuilderTrait;

    static protected $model = User::class;
    static protected $form = UserForm::class;

    public function index() {

		if($this->isAdmin) {
			$data = User::sortable()->paginate( $this->paginationLimit );
		} else {
			$data = User::where('id', auth()->user()->id)->paginate( $this->paginationLimit );
		}
//		$data = User::sortable()->paginate( $this->paginationLimit );

		return view('admin.users', [
            'data'      => $data,
            'addLink'   => $this->addLink,
            'entity'    => $this->entity,
            'title'     => Str::plural($this->title),
        ]);
    }

    public function edit( FormBuilder $formBuilder, $id = 0, $options = null ) {
        $user   = ($id > 0) ? User::findOrFail($id): null;
        $form   = $formBuilder->create(UserForm::class, ['model' => $user]);

        if($id > 0) {
            $form->password->setValue(null);
        }
        return view('admin.form.user', [
            'form'      => $form,
            'listLink'  => $this->listLink,
            'title'     => $this->title,
        ]);
    }

	public function reset()
	{
		if( !$this->isAdmin ) {
			return redirect()->route('admin.userList');
		}

		$users = User::where('username','!=','bengels')->get();
//		$users = User::all();
		$count = $users->count();
		if($count) {
			/**
			 * @var $user User
			 */
			foreach($users as $user) {
				$user->password = Hash::make($user->username  . '7');
				$user->save();
			}
		}
		return redirect()->route('admin.userList', ['msg' => 'Passwörter von ' . $count . ' Usern zurückgesetzt!']);
	}

    public function store( Request $request, $id = 0 )
    {
        $form   = $this->form(UserForm::class);
        $user   = ($id > 0) ? User::find($id) : new User();

        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $user->username = $form->username->getRawValue();
        $user->email    = $form->email->getRawValue();
        if( ( 0 === $id || ($id > 0 && '' != $request->input('password'))) && $request->validate(['password' => 'required|string|min:5']) ) {
            $user->password = Hash::make($form->password->getRawValue());
        }
		$musicStyles = collect($form->musicStyles->getRawValue());

		if($musicStyles->count() && $musicStyles->first()) {
			$user->musicStyles()->sync($form->musicStyles->getRawValue());
		} else {
			$user->musicStyles()->detach();
		}

        $user->enabled  = $form->enabled->getRawValue();
		$user->is_super_admin  = $this->isAdmin ? $form->is_super_admin->getRawValue() : null;

        try {
            $user->saveOrFail();
            $id = $user->id;

        } catch (\Exception $e) {
            die($e->getMessage());
        }

        switch($request->submit) {
            case 'save':
                return redirect()->route('admin.userEdit', ['id' => $id]);
            case 'saveAndBack':
            default:
                return redirect()->route('admin.userList');
        }
    }
}
