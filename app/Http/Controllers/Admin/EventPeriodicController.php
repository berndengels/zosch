<?php
/**
 * EventsController.php
 *
 * @author    Bernd Engels
 * @created   30.01.19 18:55
 * @copyright Webwerk Berlin GmbH
 */
namespace App\Http\Controllers\Admin;

use Auth;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Carbon\CarbonInterval;
use App\Libs\EventDateTime;
use App\Models\Theme;
use App\Models\Category;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;
use App\Models\Event;
use App\Models\Images;
use Illuminate\Http\Request;
use App\Models\EventPeriodic;
use App\Forms\EventPeriodicForm;
use App\Http\Requests\UploadRequest;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use Kris\LaravelFormBuilder\Form;
use Kris\LaravelFormBuilder\FormBuilder;
use App\Http\Controllers\Admin\MainController;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class EventPeriodicController extends MainController
{
    use FormBuilderTrait;

    static protected $model = EventPeriodic::class;
    static protected $form = EventPeriodicForm::class;

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function edit( FormBuilder $formBuilder, $id = 0, $options = null ) {
        $event  = ($id > 0) ? EventPeriodic::findOrFail($id): null;
        $form   = $formBuilder->create(EventPeriodicForm::class, ['model' => $event]);

		$dates = '';
		if($id > 0) {
			$eventDateTime = new EventDateTime();
			$dates = $eventDateTime->getPeriodicEventDates($event->periodicPosition->search_key, $event->periodicWeekday->name_en);
			array_walk($dates, function(&$v){
				$v = "'$v'";
			});
			$dates = implode(',', $dates);
		}

        return view('admin.form.eventPeriodic', [
            'id'        => $id,
            'form'      => $form,
            'dates'     => $dates,
            'title'     => $this->title,
            'listLink'  => $this->listLink,
        ]);
    }

    public function store( Request $request, $id = 0 ) {

        $form   = $this->form(EventPeriodicForm::class);
        $event  = $id > 0 ? EventPeriodic::find($id) : new EventPeriodic();

        $event->title           = $form->title->getRawValue();
        $event->subtitle        = $form->subtitle->getRawValue();
        $event->event_time      = '' !== $form->event_time->getRawValue() ? $form->event_time->getRawValue() : config('event.defaultEventTime');
        $event->description     = $form->description->getRawValue();
        $event->is_published    = isset($form->is_published) ? 1 : 0;
        $event->links           = $form->links->getRawValue();
        $event->category_id     = $form->category->getRawValue();
        $event->theme_id        = $form->theme->getRawValue();
        $event->periodic_position_id   = $request->periodicDate['position'];
        $event->periodic_weekday_id    = $request->periodicDate['weekday'];

        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $event->saveOrFail();
        $id = $event->id;

        $this->processImages($request, $id);

		switch($request->submit) {
            case 'save':
                return redirect()->route('admin.eventPeriodicEdit', ['id' => $id]);
            case 'saveAndBack':
            default:
                return redirect()->route('admin.eventPeriodicList');
        }
    }
}
