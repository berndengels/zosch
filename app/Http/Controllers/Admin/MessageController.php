<?php
/**
 * UserController.php
 *
 * @author    Bernd Engels
 * @created   28.02.19 17:17
 * @copyright Webwerk Berlin GmbH
 */
namespace App\Http\Controllers\Admin;

use App\Forms\BandsForm;
use App\Forms\Filter\MessageFilterForm;
use App\Models\Message;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Kris\LaravelFormBuilder\FormBuilder;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class MessageController extends MainController
{
    use FormBuilderTrait;

    static protected $model	= Message::class;
    static protected $form	= BandsForm::class;
//	static protected $orderBy = 'created_at';
//	static protected $orderDirection = 'desc';

	protected function getList( FormBuilder $formBuilder, Request $request )
	{
		$musicStyle = $request->get('musicStyle');
		$model = static::$model;
		$data = $model::sortable(['created_at' => 'desc'])
			->when($musicStyle, function($query) use($musicStyle) {
				return $query->where('music_style_id', $musicStyle);
			})
			->paginate( $this->paginationLimit )
		;
		$filter = $formBuilder->create(MessageFilterForm::class, [], [
			'musicStyle'=> $musicStyle,
		]);
		$data = [
			'data'      => $data,
			'filter'	=> $filter,
			'addLink'   => $this->addLink,
			'entity'    => $this->entity,
			'title'     => Str::plural($this->title),
		];
		$view = view('admin.messages', $data);
		return $view;
	}

	public function show( $id ) {
		$message = Message::find($id);

		return view('admin.messageShow', [
			'data'      => $message,
			'listLink'  => $this->listLink,
			'entity'    => $this->entity,
			'title'     => Str::plural($this->title),
		]);
	}

	public function store( Request $request )
    {
        $form   = $this->form(BandsForm::class);
        $entity = new Message();

        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $entity->name		= $form->name->getRawValue();
        $entity->email		= $form->email->getRawValue();
		$entity->message	= $form->message->getRawValue();

        try {
            $entity->saveOrFail();
            $id = $entity->id;
        } catch (\Exception $e) {
            die($e->getMessage());
        }

        switch($request->submit) {
            case 'save':
                return redirect()->route('admin.categoryEdit', ['id' => $id]);
            case 'saveAndBack':
            default:
                return redirect()->route('admin.categoryList');
        }
    }

	public function responde( $id ) {
		$message = Message::find($id);
	}
}
