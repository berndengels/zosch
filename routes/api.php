<?php

use Illuminate\Support\Facades\Request;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/
//Route::post('register', 'AuthController@register');
Route::post('login', 'ApiAuthController@login');
//Route::apiResource('events', 'ApiEventController');

Route::group([
	'prefix' => null,
], function () {
	Route::get('events/{date?}', 'ApiEventController@events');
});
