
-- INSERT IGNORE INTO zosch_prod.category (id,name,slug,default_time,is_published)
-- SELECT rub_id,rubrik,LOWER(rubrik),Uhrzeit,1 FROM zosch.rubrik ORDER BY rub_id;

INSERT IGNORE INTO zosch_prod.event (
    id,
    category_id,
    title,
    subtitle,
    description,
    links,
    event_date,
    event_time,
    created_by,
    price,
    is_published
    )
SELECT
    dat_id,
    r_id,
    Titel,
    Untertitel,
    Beschreibung,
    Link,
    Datum,
    dat_Uhrzeit,
    2,
    dat_Eintritt,
    1
FROM zosch.data ORDER BY dat_id;

UPDATE `event` SET `event_time`=REPLACE(`event_time`,'.',':');
